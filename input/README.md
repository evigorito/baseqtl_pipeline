
<!-- README.md is generated from README.Rmd. Please edit that file -->

# baseqtl input preparation

We combine command line tools with Python and R. We adapted code from
the mapping pipeline [WASP](https://github.com/bmvdgeijn/WASP) to
implement a reference panel bias correction and from
[phASER](https://github.com/secastel/phaser) for counting allele
specific reads.

## System requirements

    R versions >= 3.4.0.
    GNU make
    bcftools
    samtools
    STAR or alternative aligner
    Python v3 for estimates of reference panel bias
           * numpy
           * scipy
           * pysam version 0.8.4 or higher
           * PyTables version 3.
    Python v2 for running phASER to calculate allele specific counts
    phASER
    shapeit when working with unknown genotypes
    bedtools
    tabix

## Instalations

### bcftools, htslib (contains tabix) and samtools can be downloaded from [htslib](http://www.htslib.org/download/)

Move the downloaded file to your bin directory to build

``` bash
# uncompress
tar xvfj bcftools-x.tar.bz2
# build
cd bcftools-x (same for samtools and htslib)
./configure --prefix=/where/to/intall
make
make install
# The executable programs will be installed to a bin subdirectory
# Add bcftools/samtools path to $PATH in your .bashrc or equivalent file, otherwise R won't find bcftools/samtools
export PATH=/where/to/install/bin:$PATH
```

### Install STAR (or alternative), Python, phASER SHAPEIT and bedtools

  - To install STAR follow instructions in
    [STAR](https://github.com/alexdobin/STAR)

<!-- end list -->

``` bash
## Please make sure you use the latest release, the files that are referred in this page may not be the latest
## source code download & compile
wget https://github.com/alexdobin/STAR/archive/2.7.4a.tar.gz
tar -xzf 2.7.4a.tar.gz
cd STAR-2.7.4a/source
make STAR
```

  - To install Python2, 3 and its libraries follow
    [python](https://realpython.com/installing-python/)
  - Download and setup
    [phASER](https://stephanecastel.wordpress.com/2017/02/15/how-to-generate-ase-data-with-phaser/)

<!-- end list -->

``` bash
## Please make sure you use the latest release, the files that are referred in this page may not be the latest
## You need to install phASER dependencies and "blacklist" files [phASER](https://github.com/secastel/phaser/tree/master/phaser)
git clone https://github.com/secastel/phaser.git
```

  - To install shapeit follow
    [SHAPEIT](https://mathgen.stats.ox.ac.uk/genetics_software/shapeit/shapeit.html)

<!-- end list -->

``` bash
## Please make sure you use the latest release, the files that are referred in this page may not be the latest
wget https://mathgen.stats.ox.ac.uk/genetics_software/shapeit/shapeit.v2.r904.glibcv2.17.linux.tar.gz
tar -zxvf shapeit.v2.r904.glibcv2.17.linux.tar.gz
```

  - Follow
    [bedtools](https://bedtools.readthedocs.io/en/latest/content/installation.html)
    for
installation

<!-- end list -->

``` bash
## Please make sure you use the latest release, the files that are referred in this page may not be the latest
wget https://github.com/arq5x/bedtools2/releases/download/v2.29.2/bedtools-2.29.2.tar.gz
tar -xvf bedtools-2.29.2.tar.gz
cd bedtools2
## compile 
make
```

### Install inputs4baseqtl from [GitLab](https://gitlab.com):

``` r
## Within R:
## "biomaRt","GenomicFeatures","GenomicAlignments" need to be installed
## they can be installed by:
if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
BiocManager::install(c("biomaRt","GenomicFeatures","GenomicAlignments")))

install.packages("devtools") # if you don't already have the package
library(devtools)
devtools::install_gitlab("evigorito/input4baseqtl")
```

### Clone the baseqtl\_pipeline repository, if you haven’t done it yet

``` bash
# clone the baseqtl_pipeline repository
 git clone https://gitlab.com/evigorito/baseqtl_pipeline.git
 
```

### External data

  - Reference panel We have used the [1000 Genomes
    Phase3](https://mathgen.stats.ox.ac.uk/impute/1000GP_Phase3.html).
    The reference panel of choice should be in the hap/legend/sample
    format.

<!-- end list -->

``` bash
# Example files
wget https://mathgen.stats.ox.ac.uk/impute/1000GP_Phase3.tgz
tar -zvxf 1000GP_Phase3.tgz
 
```

  - GRCh37 primary assembly &
GTF

<!-- end list -->

``` bash
wget ftp://ftp.ensembl.org/pub/grch37/current/fasta/homo_sapiens/dna/Homo_sapiens.GRCh37.dna.primary_assembly.fa.gz
gunzip Homo_sapiens.GRCh37.dna.primary_assembly.fa.gz
wget ftp://ftp.ensembl.org/pub/grch37/release-87/gtf/homo_sapiens/Homo_sapiens.GRCh37.87.gtf.gz
gunzip Homo_sapiens.GRCh37.87.gtf.gz
 
```

  - GRch37 reference fasta used in bcftools for RNA variant
callings

<!-- end list -->

``` bash
wget http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/technical/reference/human_g1k_v37.fasta.gz
 
```

  - Example data: Downloads metadata for whole GEUVADIS
samples

<!-- end list -->

``` bash
wget ftp.ebi.ac.uk/pub/databases/arrayexpress/data/experiment/GEUV/E-GEUV-1/E-GEUV-1.sdrf.txt
 
```

### Working example

We provide a
[snakemake](https://snakemake.readthedocs.io/en/stable/tutorial/tutorial.html)
pipeline describing the steps of the ppeline. The working example
reproduces the input generation for the set of 86 samples from the
GEUVADIS project we used in the paper [paper](link).

**You will find the following files and directories in [input]()**

1.  The
    [Snakefile](https://gitlab.com/evigorito/baseqtl_pipeline/-/blob/master/input/Snakefile)
    provides a user-friendly syntax that can be easily understood and
    written in command line for non-Python users. A brief description on
    how it is organized can be found below.

2.  The
    [config.yaml](https://gitlab.com/evigorito/baseqtl_pipeline/-/blob/master/input/config.yaml)
    file list the paths to tools (STAR, phASER, etc), external files
    (reference panel, genomic annotations, etc), and output directory to
    guide the Snakefile. This file needs to be edited with your own
    locations. The **refbias** path in the config.yaml file is the path
    to
    [refbias](https://gitlab.com/evigorito/baseqtl_pipeline/-/tree/master/input/refbias)
    directory in
    [input](https://gitlab.com/evigorito/baseqtl_pipeline/-/tree/master/input),
    expalined below.

3.  The
    [cpu.json](https://gitlab.com/evigorito/baseqtl_pipeline/-/blob/master/input/cpu.json)
    file shows an example for setting up parameters for the snakemake
    rules when using the slurm queuing system for job submission.

4.  The
    [data](https://gitlab.com/evigorito/baseqtl_pipeline/-/tree/master/input/data)
    dir contains [samples]() which is a file with the ids of the 86
    GEUVADIS GBR samples used in the analysis.

5.  The
    [refbias](https://gitlab.com/evigorito/baseqtl_pipeline/-/tree/master/input/refbias)
    dir contains the Python3 code to apply reference panel bias
    correction. For using the scrips in this directory you call python
    on this directory. For example, to see a description of AI.py try
    python /path/to/repo/inputs/refbias/AI.py –help. Is using the
    Snakefile, the path of this directory needs to go in config.yaml
    refbias entry.

6.  The
    [Scripts](https://gitlab.com/evigorito/baseqtl_pipeline/-/tree/master/input/Scripts)
    directory contains the R scripts that are called in the pipeline.

### Snakefile structure

1.  Set access to bashrc file and import relevant python modules
2.  Helper functions to run rules:
      - sample\_id: creates a list of sample ids from file
      - sample\_ftp: makes a data frame from metadata file to match
        samples to ftp files
      - sample\_fastq: makes a data frame from metadata to match
        sample\_id with fastq files. When paired design each sample is
        matched to 2 files
3.  Rules for downloading and aligning RNA-seq files
4.  Rules for calculating total gene counts and library size
5.  When working with **genotypes** prepare a vcf file with phased
    genotypes. The example uses sample/haplotype/legend input files
6.  When working with **unknown genotypes** a set of rules is provided
    to call variants from RNA-seq and prepare a vcf file with phased
    gentoypes
7.  Rules for quantifying allele specific expression and preparing a vcf
    file with GT:ASE information.
8.  Rule for matching exonic SNPs to genes.
9.  Rules for making estimates of reference panel bias for exonic SNPs.
