
<!-- README.md is generated from README.Rmd. Please edit that file -->

# BaseQTL pipeline

This pipeline is designed for running
[BaseQTL](https://gitlab.com/evigorito/baseqtl) an R pacakge with a
suite of models to discover eQTLs using allele specfic expresion in a
Bayesian framework. The pipeline is divided in two independent
    parts:

1.  [input](https://gitlab.com/evigorito/baseqtl_pipeline/-/tree/master/input)
    preparation. Starting from RNA-seq fastq files and a vcf file with
    genotypes (when avaliable) the section describes how to prepare all
    input files required for
    [baseqtl](https://gitlab.com/evigorito/baseqtl).

2.  [baseqtl](https://gitlab.com/evigorito/baseqtl) R package for
    mapping eQTL

## Instalation

Clone the repository

``` bash
# clone the baseqtl_pipeline repository
 git clone https://gitlab.com/evigorito/baseqtl_pipeline.git
 
```
